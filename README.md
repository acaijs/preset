# Açaí General Config Preset

This package serves as a reference to create configuration files for any and all Açaí projects, including modules, boilerplates and such.

# Configs included
- eslint
- tsconfig